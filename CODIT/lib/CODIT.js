'use babel';

// tableau listant les connexions actives
let listeConnexions=[]


// tableau pour stocker les marker pour la sélection
let listeMarker=[]

import MonCurseurView from './CODIT-view';
import { CompositeDisposable } from 'atom';

export default {

  monCurseurView: null,
  modalPanel: null,
  subscriptions: null,

  activate(state) {
    this.monCurseurView = new MonCurseurView(state.monCurseurViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.monCurseurView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'CODIT:startConnection': () => this.startConnection(),
      'CODIT:stopConnection': () => this.stopConnection()
    }));
  },


  // fonction permettant de stopper la connexion avec le serveur
  stopConnection(){
    if (listeConnexions.length==0){
      atom.notifications.addError('Erreur : Aucune connexion ouverte !')
    }
    else{
      listeConnexions[0].close();
      listeConnexions.pop();
      atom.notifications.addSuccess('Déconnexion au serveur réussie !')
    }

  },

  deactivate() {
    this.modalPanel.destroy();
    this.subscriptions.dispose();
    this.monCurseurView.destroy();
  },

  serialize() {
    return {
      monCurseurViewState: this.monCurseurView.serialize()
    };
  },


  // fonction permettant de démarrer la connexion avec le serveur
  startConnection() {

    // TODO : créer une interface de connexion

    // création de la socket permettant d'échanger avec le serveur central

    // TODO : adapter la connexion à l'adresse entrée par l'utilisateur


    // création de la socket pour se connecter au serveur
    const
        io = require("socket.io-client"),
        ioClient = io.connect("http://127.0.0.1:8000");


    // à la réception d'un message d'erreur (si le serveur n'existe pas/n'est pas dispo)
    ioClient.io.on("connect_error", (msg) => {
        atom.notifications.addError('Serveur introuvable')
  	})

    // à la réussite de la connexion avec le serveur
    ioClient.on("connect",  function(socket){
      console.log("Connexion au serveur réussie !")
      atom.notifications.addSuccess('Connexion au serveur réussie !')
      ioClient.emit("join","")
    })

    // ajout de la connexion au tableau des différentes connexions
    // TODO : trouver un moyen plus propre de réaliser la déconnexion au serveur
    listeConnexions.push(ioClient);

    // variable définissant la fenêtre active de l'éditeur Atom
    let editor
    if (editor = atom.workspace.getActiveTextEditor()) {

      // variable contenant la valeur du curseur de l'autre utilisateur
      let autreCurseur="0,0"

      // blocage permettant de trigger l'ajout si et seulement si on est à distance
      // comme un mutex
      let blocageAjout=0
      let blocageSuppression=0
      let blocageRemplacement=0


      // création du curseur (affichage) de l'autre utilisateur en HTML
      var element = document.createElement('span')
      element.className="cursor"
      element.style="height: 1.5em; width: 8.23438px; border-color:#8e44ad;margin-top: -1.5em"

      // marqueur pour placer le curseur (et le déplacer)
      var marker = editor.markScreenPosition([0,0])

      // placement du curseur et définission de son type
      editor.decorateMarker(marker, {type: 'overlay', item: element})

      // variable permettant de ne mettre à jour que son propre curseur
      let blocage=0


      // trigger de la suppression de caractère

      // buffer qui contient l'ensemble du texte de l'éditeur
      let buffer1 = editor.getBuffer()

      buffer1.onDidChange((msg)=>{

        let tailleSuppression = msg.oldRange.end.column - msg.oldRange.start.column
        let suppressionCondition1 = msg.newRange.end.row == msg.newRange.start.row && msg.newRange.end.column == msg.newRange.start.column
        let suppressionCondition2 = msg.newText==""
        //console.log(msg.newText)
        let suppressionCaractereCondition = tailleSuppression == 1

        let ajoutCondition = msg.oldText==""

        // // s'il s'agit d'une suppresion d'un caractère, on envoit l'ordre associé au serveur
        // if (suppressionCondition1 && suppressionCondition2 && suppressionCaractereCondition){
        //   console.log("suppression de caractère !!")
        //   //ioClient.emit("delCharUser1","suppression")
        //   if (blocageSuppression == 0){
        //     console.log(msg)
        //     ioClient.emit("update", {"operation": {"type": "delete_char", "position": {"line": 12, "column": 0}, "text": "b"}})
        //   }
        // }

        // s'il s'agit d'une suppresion d'un bloc de caractère, on envoit l'ordre associé au serveur
        if (suppressionCondition1 && suppressionCondition2){
          //console.log("suppression!!")
          //ioClient.emit("delCharUser1","suppression")
          if (blocageSuppression == 0){
            //console.log(msg)
            ioClient.emit("update", { "operation": {"type": "delete", "range": { "start":{"line": msg.oldRange.start.row, "column": msg.oldRange.start.column},"end":{"line": msg.oldRange.end.row, "column": msg.oldRange.end.column}}}})
          }
        }

        // si on a une insertion de caractère
        if (ajoutCondition && !suppressionCondition2){
          //console.log("insertion de texte")

          if (blocageAjout == 0){
            ioClient.emit("update", { "operation": {"type": "insert_char", "position": {"line": msg.oldRange.end.row, "column": msg.oldRange.end.column}, "text": msg.newText}})
          }
        }


        // si on a un remplacement de chaine de caractère

        if (msg.oldText !="" && msg.newText!=""){
          if (blocageRemplacement == 0){
            ioClient.emit("update", { "operation": {"type": "replace", "range": { "start":{"line": msg.oldRange.start.row, "column": msg.oldRange.start.column},"end":{"line": msg.oldRange.end.row, "column": msg.oldRange.end.column}}, "text": msg.newText}})
          }
        }

      })

      // dès qu'un changement de position du curseur est constaté, on l'envoie au serveur
      editor.onDidChangeCursorPosition((msg) => {
        if (blocage==0){
          let positionY=msg.newScreenPosition.row.toString()
          let positionX=msg.newScreenPosition.column.toString()
          //console.log(positionY+","+positionX)
          //ioClient.emit("cursorPositionUser1",positionY+","+positionX)
          ioClient.emit("update", { "cursor": {"line": positionY, "column": positionX}})
        }
      })

      ioClient.on("update", (msg) => {

        //console.log(msg)

        //s'il s'agit d'un changement de position de curseur
        // Changement de la position du curseur de l'autre personne dès que le serveur
        // envoie l'information
        if (msg.cursor != undefined){

          // récupération des coordonnées du curseur de l'autre utilisateur
          autreCurseur=msg.cursor.line+","+msg.cursor.column

          // définition des coordonnées du curseur de l'autre utilisateur
          coordonneeLigne=parseInt(msg.cursor.line)
          coordonneeColonne=parseInt(msg.cursor.column)

          // changement de position du marqueur sur lequel est placé le 2eme curseur
          marker.setHeadScreenPosition([coordonneeLigne,coordonneeColonne])
        }

        // s'il s'agit d'une opération qui va modifier le texte
        if (msg.operation != undefined){

          // insertion d'unn caractère/un bloc de caractères
          if (msg.operation.type=="insert_char"){
            //console.log("insertion de caractère")
            blocageAjout=1
            // mise à 1 du blocage, pour éviter d'envoyer un changement de position de curseur
            // au serveur
            blocage=1
            //console.log(msg.operation.position.line)
            //console.log(msg.operation.position.column)
            buffer1.insert([msg.operation.position.line,msg.operation.position.column], msg.operation.text)
            blocage=0
            blocageAjout=0
          }

          // // suppression d'un caractère
          // if (msg.operation.type=="delete_char"){
          //   console.log("suppression de caractère")
          //   // mise à 1 du blocage, pour éviter d'envoyer un changement de position de curseur
          //   // au serveur
          //   blocage=1
          //   blocageSuppression = 1
          //   // récupération de la position actuelle de notre curseur
          //   currentPosition = editor.getCursorScreenPosition()
          //
          //   // définition des coordonnées du curseur de l'autre utilisateur
          //   coordonneeX=parseInt(autreCurseur.split(",")[0])
          //   coordonneeY=parseInt(autreCurseur.split(",")[1])
          //
          //   // déplacement de notre curseur à la position de l'autre utilisateur
          //   editor.setCursorScreenPosition([coordonneeX,coordonneeY])
          //   editor.delete()
          //
          //   // déplacement de notre curseur à son emplacement d'origine
          //   editor.setCursorScreenPosition(currentPosition)
          //   blocageSuppression = 0
          //   blocage=0
          // }

          // suppression d'un bloc de texte
          if (msg.operation.type=="delete"){
            blocage=1
            //console.log("suppression de caractère")
            blocageSuppression = 1
            buffer1.delete([[msg.operation.range.start.line,msg.operation.range.start.column],[msg.operation.range.end.line,msg.operation.range.end.column]])
            blocageSuppression = 0
            blocage=0
          }


          // s'il s'agit d'une sélection
          if (msg.operation.type=="selection"){

            if (listeMarker[0]!=undefined){
              listeMarker[0].destroy();
              listeMarker.pop();
            }

            // condition pour voir si une sélection est vide
            let conditionVide = msg.operation.range.start.line == msg.operation.range.end.line && msg.operation.range.start.column == msg.operation.range.end.column

            if (!conditionVide){

              let marker = editor.markBufferRange([[msg.operation.range.start.line,msg.operation.range.start.column],[msg.operation.range.end.line,msg.operation.range.end.column]])
              listeMarker.push(marker);
              //console.log(marker)
              editor.decorateMarker(listeMarker[0], {type: 'highlight', class: 'ma-classe'});
            }

          }

          if (msg.operation.type=="replace"){
            blocage=1
            blocageRemplacement=1
            buffer1.setTextInRange([[msg.operation.range.start.line,msg.operation.range.start.column],[msg.operation.range.end.line,msg.operation.range.end.column]], msg.operation.text)
            blocageRemplacement=0
            blocage=0
          }


        }
      });

      // pour détecter un changement de sélection
      editor.onDidChangeSelectionRange((msg) => {
        //console.log(msg);
        ioClient.emit("update", { "operation": {"type": "selection", "range": { "start":{"line": msg.newBufferRange.start.row, "column": msg.newBufferRange.start.column},"end":{"line": msg.newBufferRange.end.row, "column": msg.newBufferRange.end.column}}}})
      })


    }


  }

};
