'use babel';

import MonCurseur from '../lib/CODIT';

// Use the command `window:run-package-specs` (cmd-alt-ctrl-p) to run specs.
//
// To run a specific `it` or `describe` block add an `f` to the front (e.g. `fit`
// or `fdescribe`). Remove the `f` to unfocus the block.

describe('MonCurseur', () => {
  let workspaceElement, activationPromise;

  beforeEach(() => {
    workspaceElement = atom.views.getView(atom.workspace);
    activationPromise = atom.packages.activatePackage('CODIT');
  });

  describe('when the CODIT:startConnection event is triggered', () => {
    it('hides and shows the modal panel', () => {
      // Before the activation event the view is not on the DOM, and no panel
      // has been created
      expect(workspaceElement.querySelector('.CODIT')).not.toExist();

      // This is an activation event, triggering it will cause the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'CODIT:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        expect(workspaceElement.querySelector('.CODIT')).toExist();

        let monCurseurElement = workspaceElement.querySelector('.CODIT');
        expect(monCurseurElement).toExist();

        let monCurseurPanel = atom.workspace.panelForItem(monCurseurElement);
        expect(monCurseurPanel.isVisible()).toBe(true);
        atom.commands.dispatch(workspaceElement, 'CODIT:toggle');
        expect(monCurseurPanel.isVisible()).toBe(false);
      });
    });

    it('hides and shows the view', () => {
      // This test shows you an integration test testing at the view level.

      // Attaching the workspaceElement to the DOM is required to allow the
      // `toBeVisible()` matchers to work. Anything testing visibility or focus
      // requires that the workspaceElement is on the DOM. Tests that attach the
      // workspaceElement to the DOM are generally slower than those off DOM.
      jasmine.attachToDOM(workspaceElement);

      expect(workspaceElement.querySelector('.CODIT')).not.toExist();

      // This is an activation event, triggering it causes the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'CODIT:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        // Now we can test for view visibility
        let monCurseurElement = workspaceElement.querySelector('.CODIT');
        expect(monCurseurElement).toBeVisible();
        atom.commands.dispatch(workspaceElement, 'CODIT:toggle');
        expect(monCurseurElement).not.toBeVisible();
      });
    });
  });
});
