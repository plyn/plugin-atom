# Plugin d'édition collaborative inter IDE

Le plugin suivant, destiné à être installé sur l'IDE Atom, permet l'édition collaborative à plusieurs sur le même fichier de code.

## Pré-réquis

Installation de la bibliothèque socket.io : 

```
npm install socket.io
```

## Licence



