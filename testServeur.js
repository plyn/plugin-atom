const
    io = require("socket.io"),
    server = io.listen(8000);


// création d'une correspondance (dictionnaire) entre le numéro de séquence et le client
let
    sequenceNumberByClient = new Map();

// event fired every time a new client connects:
server.on("connection", (socket) => {
    console.info(`Client connected [id=${socket.id}]`);
    // initialize this client's sequence number
    sequenceNumberByClient.set(socket, 1);

    // when socket disconnects, remove it from the list:
    socket.on("disconnect", () => {
        sequenceNumberByClient.delete(socket);
        console.info(`Client gone [id=${socket.id}]`);
    });


    socket.on("update", (msg) => {
        //console.info(msg)
		//console.info(`[id=${socket.id}]`);
		socket.broadcast.emit("update", msg);
		
    })


});




// sends each client its current sequence number
//setInterval(() => {
    // for (const [client, sequenceNumber] of sequenceNumberByClient.entries()) {
    //     client.emit("seq-num", sequenceNumber);
    //     sequenceNumberByClient.set(client, sequenceNumber + 1);
    // }
/*     let position='1,'+Math.floor(Math.random() * Math.floor(4)).toString(10)
    if (position == "1,0" ){server.emit('cursorPosition', position)}
    else if (position == "1,1" ){server.emit('cursorPosition', position)}
	else if (position == "1,4" ){server.emit('cursorPosition', position)}
    else{server.emit('AddChar','B')} */

    //server.emit('delChar', '');

    // server.emit('cursorPosition', '1,2');
    // server.emit('AddChar','B')
    // server.emit('cursorPosition', '1,3');
    // server.emit('DelChar','');
    // server.emit('cursorPosition', '1,4');
//}, 1000);
